import React, { useState } from 'react'
import { Link, useHistory } from 'react-router-dom'

import Button from 'react-bootstrap/lib/Button'
import Modal from 'react-bootstrap/lib/Modal'
import Collapse from 'react-bootstrap/lib/Collapse'
import Tabs from 'react-bootstrap/lib/Tabs'
import Tab from 'react-bootstrap/lib/Tab'
import Glyphicon from 'react-bootstrap/lib/Glyphicon'


const TermsAndPrivacyModal = () => {
    //for modal
    const [showModal, setShowModal] = useState(false);
    const closeModal = () => setShowModal(false);
    const openModal = () => setShowModal(true);

    //for panel
    const [collapse, setCollapse] = useState(false)


    //state for checkbox
    const [checked, setChecked] = useState(false);

    //routing to register page
    const history = useHistory();
    const routeToRegister = (event) => {
        // event.preventDefault()
        history.push("register")
    }
    return (
        <>
            <Link onClick={openModal}>Create an Account?</Link>
            <Modal show={showModal}
                onHide={closeModal}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
            >
                <Modal.Header style={modalHeader}>
                    Terms &amp; Privacy Policy
			</Modal.Header>
                <Modal.Body style={modalBody}>
                    <div style={soiDiv}>
                        <Button
                            onClick={() => setCollapse(!collapse)}
                            aria-controls="Statement-of-intent"
                            aria-expanded={collapse}
                            block
                            style={{ border: 0, borderRadius: "0.2em", backgroundColor: "#EEEEEE" }}
                        >
                            <Glyphicon glyph="info-sign" style={{ float: "left" }} />
                            <span>Statement of intent</span>
                            <Glyphicon glyph="chevron-down" style={{ float: "right", transform: collapse && "rotate(180deg)", transition: "transform 0.6s" }} />
                        </Button>
                        <Collapse in={collapse}>
                            <div id="Statement-of-intent">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
                                terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer
                                labore wes anderson cred nesciunt sapiente ea proident.
                            </div>
                        </Collapse>
                    </div>

                    <Tabs defaultActiveKey={0} id="tabs">
                        <Tab eventKey={0} title="Terms of Use">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla rutrum laoreet nisl, quis egestas lacus hendrerit et. Integer feugiat est iaculis dui luctus dapibus quis eu dolor. Cras fringilla, odio ac imperdiet bibendum, nulla lacus mollis ligula, iaculis rutrum augue massa sit amet quam. Nulla ut augue eget nulla elementum volutpat. Aliquam justo nibh, feugiat ut volutpat ut, elementum vel lorem. Pellentesque vel ante tortor. In venenatis tortor risus, vulputate vulputate neque accumsan condimentum.

                                Proin cursus nisl vel nunc tristique elementum. Quisque cursus maximus ultricies. Proin vel odio at eros laoreet congue nec mollis tellus. Vivamus finibus rutrum mauris, quis hendrerit diam vulputate non. Integer eget consequat tortor. Integer blandit tellus purus, eget pretium ipsum lacinia eget. Nunc condimentum magna nec lectus vehicula, ac ornare libero imperdiet.

                                Phasellus sit amet eleifend magna, ac tempus quam. Proin facilisis lobortis nulla vel euismod. Sed eget metus mauris. Aenean est nunc, sodales non placerat lobortis, dapibus ac nibh. Praesent imperdiet neque a efficitur ullamcorper. Morbi scelerisque feugiat magna, et mollis odio fermentum ut. Aliquam in diam dui.

                                Nulla egestas id eros ut euismod. Sed nunc turpis, tincidunt vel enim convallis, vulputate tempor enim. Donec in dolor dolor. Suspendisse ultrices tellus quis accumsan maximus. Suspendisse finibus, ipsum eget volutpat tempus, nisi justo auctor enim, nec pretium tellus ipsum sollicitudin libero. Etiam vitae urna commodo, tempor eros vitae, rutrum lectus. Ut fringilla orci nec libero luctus pellentesque. Vivamus in risus eu lacus maximus lacinia sed at lectus.

                                Fusce fringilla viverra sapien, in dignissim neque tempus ac. Proin pretium lacus felis, in efficitur orci consectetur ac. Praesent a erat lacus. Aliquam elementum rutrum pellentesque. Fusce facilisis lorem ligula, at consectetur quam porttitor a. Phasellus tincidunt felis sed gravida condimentum. Quisque magna odio, dapibus quis risus quis, blandit feugiat lorem.
                            </p>
                        </Tab>
                        <Tab eventKey={1} title="Privacy Policy">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla rutrum laoreet nisl, quis egestas lacus hendrerit et. Integer feugiat est iaculis dui luctus dapibus quis eu dolor. Cras fringilla, odio ac imperdiet bibendum, nulla lacus mollis ligula, iaculis rutrum augue massa sit amet quam. Nulla ut augue eget nulla elementum volutpat. Aliquam justo nibh, feugiat ut volutpat ut, elementum vel lorem. Pellentesque vel ante tortor. In venenatis tortor risus, vulputate vulputate neque accumsan condimentum.

                                Proin cursus nisl vel nunc tristique elementum. Quisque cursus maximus ultricies. Proin vel odio at eros laoreet congue nec mollis tellus. Vivamus finibus rutrum mauris, quis hendrerit diam vulputate non. Integer eget consequat tortor. Integer blandit tellus purus, eget pretium ipsum lacinia eget. Nunc condimentum magna nec lectus vehicula, ac ornare libero imperdiet.

                                Phasellus sit amet eleifend magna, ac tempus quam. Proin facilisis lobortis nulla vel euismod. Sed eget metus mauris. Aenean est nunc, sodales non placerat lobortis, dapibus ac nibh. Praesent imperdiet neque a efficitur ullamcorper. Morbi scelerisque feugiat magna, et mollis odio fermentum ut. Aliquam in diam dui.

                                Nulla egestas id eros ut euismod. Sed nunc turpis, tincidunt vel enim convallis, vulputate tempor enim. Donec in dolor dolor. Suspendisse ultrices tellus quis accumsan maximus. Suspendisse finibus, ipsum eget volutpat tempus, nisi justo auctor enim, nec pretium tellus ipsum sollicitudin libero. Etiam vitae urna commodo, tempor eros vitae, rutrum lectus. Ut fringilla orci nec libero luctus pellentesque. Vivamus in risus eu lacus maximus lacinia sed at lectus.

                                Fusce fringilla viverra sapien, in dignissim neque tempus ac. Proin pretium lacus felis, in efficitur orci consectetur ac. Praesent a erat lacus. Aliquam elementum rutrum pellentesque. Fusce facilisis lorem ligula, at consectetur quam porttitor a. Phasellus tincidunt felis sed gravida condimentum. Quisque magna odio, dapibus quis risus quis, blandit feugiat lorem.

                            </p>
                        </Tab>
                    </Tabs>

                </Modal.Body>
                <Modal.Footer>
                    <div>
                        <input type="checkbox" checked={checked} onChange={() => setChecked(!checked)} style={{ marginRight: "0.3em" }} />
                        <span>I have read and agree to the Terms and Privacy Policy</span>
                    </div>

                    <Button onClick={closeModal} style={modalButton}>
                        Close
				</Button>
                    <Button onClick={routeToRegister} disabled={!checked} style={modalButton}>
                        Accept
				</Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}


//styles for this component
const soiDiv = {
    backgroundColor: "#EEEEEE",
    borderRadius: "0.2em",
    padding: "0.2em",
    marginBottom: "0.3em",
}

const modalBody = {
    height: "60vh",
    overflowY: "scroll"
}

const modalHeader = {
    borderTopLeftRadius: "0.8em",
    borderTopRightRadius: "0.8em"
}

const modalButton = {
    borderRadius: 50
}
export default TermsAndPrivacyModal